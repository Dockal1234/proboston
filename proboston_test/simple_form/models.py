from django.db import models

class Contact(models.Model):
    name = models.CharField(max_length=255)
    email = models.EmailField(blank=True)
    ico = models.CharField(max_length=8)

    def __str__(self):
        return self.name