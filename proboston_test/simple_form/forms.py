from django.forms import ModelForm, ValidationError
from .models import Contact
from . import validators


class SimpleForm(ModelForm):

    class Meta:
        model = Contact
        fields = ['name', 'email', 'ico']

    def clean(self):
        cleaned_data = super(SimpleForm, self).clean()
        ico = cleaned_data.get("ico")

        validators.validate_ico(ico)
          #  print("ouky douky")
        #else:
           # raise ValidationError("Zadejte platne ico3!")

        return cleaned_data
