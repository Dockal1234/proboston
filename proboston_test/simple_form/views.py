from django.shortcuts import render
from django.http import HttpResponseRedirect

from .forms import SimpleForm


def index(request):
    if request.method == "POST":
        form = SimpleForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect("")
    else:
        form = SimpleForm()
    return render(request, 'form.html', {'form': form})
