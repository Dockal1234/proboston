from urllib.request import urlopen
from django.core.exceptions import ValidationError
from xml.etree import ElementTree as etree

def validate_ico(value):
    # raise exception if ico value is not a number
    try:
        ico = int(value)
    except:
        raise ValidationError("Zadejte platne ico1!")
    # valuates ico in ares
    url = 'http://wwwinfo.mfcr.cz/cgi-bin/ares/darv_std.cgi?ico={}&jazyk=cz&xml=1&aktivni=false'.format(ico)
    xml_ares = urlopen(url)
    data = xml_ares.read()
    xml_ares.close()
    str_data = str(data, 'utf-8')
    root = etree.fromstring(str_data)
    if int(root[0][0].text) == 0:
        raise ValidationError("Zadejte platne ico2!")






